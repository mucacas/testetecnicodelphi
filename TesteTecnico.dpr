program TesteTecnico;

uses
  Vcl.Forms,
  unit_principal in 'Fontes\unit_principal.pas' {form_principal},
  unit_dados in 'Fontes\unit_dados.pas' {form_dados: TDataModule},
  unit_login in 'Fontes\unit_login.pas' {form_login},
  unit_configurar_servidor in 'Fontes\unit_configurar_servidor.pas' {form_configurar_servidor},
  classe.conexao in 'Fontes\classe.conexao.pas',
  unit_mensagens in 'Fontes\unit_mensagens.pas' {form_mensagens},
  System.SysUtils {form_mensagens},
  unit_funcoes in 'Fontes\unit_funcoes.pas',
  unit_pedido_venda in 'Fontes\unit_pedido_venda.pas' {form_pedido_venda},
  classe.clientes in 'Fontes\classe.clientes.pas',
  classe.produtos in 'Fontes\classe.produtos.pas',
  unit_clientes in 'Fontes\unit_clientes.pas' {form_clientes},
  unit_produtos in 'Fontes\unit_produtos.pas' {form_produtos},
  classe.pedido_venda in 'Fontes\classe.pedido_venda.pas',
  unit_consulta_vendas in 'Fontes\unit_consulta_vendas.pas' {form_consulta_vendas};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;

  Application.CreateForm(Tform_dados, form_dados);

  if form_dados.Conexao.fnc_conectar_banco_dados then
  Begin
    form_login:= Tform_login.Create(nil);
    form_login.ShowModal;

    Application.CreateForm(Tform_principal, form_principal);

    form_login.Hide;
    form_login.Free;

    Application.Run;
  end else
  begin
      fnc_criar_mensagem( 'CONEX�O AO BANCO DE DADOS',
                          'ERRO AO CONECTAR AO BANCO DE DADOS',
                          'N�o foi poss�vel conectar ao Banco de Dados, poss�vel causa: ' + form_dados.Conexao.MsgErro,
                          'OK' );
      Application.CreateForm(Tform_configurar_servidor, form_configurar_servidor);
      form_configurar_servidor.ShowModal;
  end;
end.
