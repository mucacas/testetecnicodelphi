unit unit_login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.Imaging.pngimage, unit_configurar_servidor;

type
  Tform_login = class(TForm)
    pnl_fundo: TPanel;
    pnl_lateral: TPanel;
    lbl_bemvindo: TLabel;
    lbl_nome_sistema: TLabel;
    lbl_versao: TLabel;
    lbl_autor: TLabel;
    lbl_titulo: TLabel;
    lbl_nome_usuario: TLabel;
    pnl_usuario: TPanel;
    Edit1: TEdit;
    pnl_linha_usuario: TPanel;
    pnl_senha: TPanel;
    lbl_senha: TLabel;
    Edit2: TEdit;
    pnl_linha_senha: TPanel;
    pnl_btn_confirmar: TPanel;
    btn_confirmar: TSpeedButton;
    btn_fechar: TSpeedButton;
    Image1: TImage;
    procedure btn_fecharClick(Sender: TObject);
    procedure btn_confirmarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_login: Tform_login;

implementation
{$R *.dfm}

uses unit_principal;

procedure Tform_login.btn_confirmarClick(Sender: TObject);
begin
    form_principal:= Tform_principal.Create(nil);
    form_principal.ShowModal;
end;

procedure Tform_login.btn_fecharClick(Sender: TObject);
begin
    Application.Terminate;
end;


procedure Tform_login.FormActivate(Sender: TObject);
begin
    pnl_fundo.Left := Round ( (form_login.Width - pnl_fundo.Width) / 2 );
    pnl_fundo.Top  := Round ( (form_login.Height - pnl_fundo.Height) / 2 );
end;

end.
