unit unit_dados;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, Data.DB,
  FireDAC.Comp.Client, classe.conexao, classe.clientes, classe.produtos,
  classe.pedido_venda;

type
  Tform_dados = class(TDataModule)
    FDConnection: TFDConnection;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    MySQLDriverLink: TFDPhysMySQLDriverLink;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    Conexao : Tconexao;
    Clientes : TClientes;
    Produtos : TProdutos;
    Pedido_Venda : TPedido_Venda;
  end;

var
  form_dados: Tform_dados;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}


{$R *.dfm}

procedure Tform_dados.DataModuleCreate(Sender: TObject);
begin
  Conexao := TConexao.Create(FDConnection);
end;

procedure Tform_dados.DataModuleDestroy(Sender: TObject);
begin
  Conexao.Destroy;
end;

end.
