unit classe.conexao;

interface

uses
  FireDAC.Comp.Client, FireDAC.Stan.Intf, System.SysUtils, System.IniFiles, Vcl.Forms;
    type
      Tconexao= class
        private
          Fservidor: String;
          FMsgErro: String;
          FSenha: String;
          FBase: String;
          FLogin: String;
          FPorta: String;
          FConexao: TFDConnection;
        public
          constructor Create (NomeConexao : TFDConnection);
          destructor Destroy; override;

          Function fnc_Ler_Arquivo_INI : boolean;
          function fnc_conectar_banco_dados : boolean;
          procedure prc_Gravar_Arquivo_INI;

          property Conexao : TFDConnection Read FConexao Write FConexao;
          property Servidor: String Read FServidor Write FServidor;
          property Base    : String Read FBase Write FBase;
          property Login   : String Read FLogin Write FLogin;
          property Senha   : String Read FSenha Write FSenha;
          property Porta   : String Read FPorta Write FPorta;
          property MsgErro : String Read FMsgErro Write FMsgErro;
    end;
implementation



{ Tconexao }



constructor Tconexao.Create(NomeConexao: TFDConnection);
begin
  Fconexao := NomeConexao;
end;

destructor Tconexao.Destroy;
begin
  Fconexao.Connected := false;
  inherited;
end;

function Tconexao.fnc_conectar_banco_dados: boolean;
begin
  Result:= true;

  Fconexao.Params.Clear;

  if not fnc_Ler_Arquivo_INI then
  begin
    result := False;
    FmsgErro := 'O arquivo de Configura��o n�o foi encontrado!';
  end else
  begin
    Fconexao.Params.add('Server='+ FServidor);
    Fconexao.Params.add('user_name=' + FLogin);
    Fconexao.Params.add('password=' + FSenha);
    Fconexao.Params.add('port=' + FPorta);
    Fconexao.Params.add('Database=' + FBase);
    Fconexao.Params.add('DriverID=' + 'MySQL');

    try
      Fconexao.Connected := True;
    Except
      on e:Exception do
        begin
          FMsgErro := e.Message;
          Result   := False;
        end;
    end;
  end;
end;

procedure Tconexao.prc_Gravar_Arquivo_INI;
var
  IniFile: String;
  Ini    : TiniFile;
begin
  IniFile := ChangeFileExt (Application.Exename,'.ini');
  ini     := TiniFile.Create(IniFile);
  try
    ini.WriteString('Configuracao','Servidor',FServidor);
    ini.WriteString('Configuracao','Base',FBase);
    ini.WriteString('Configuracao','Porta',FPorta);
    ini.WriteString('Acesso','Login',FLogin);
    ini.WriteString('Acesso','Senha',FSenha);

  finally
    ini.free;
  end;
end;

Function Tconexao.fnc_Ler_Arquivo_INI: boolean;
var
  IniFile: String;
  Ini    : TiniFile;
begin

  IniFile := ChangeFileExt (Application.Exename,'.ini');
  ini     := TiniFile.Create(IniFile);
  if fileexists(IniFile) then
    Result := False
  else
  begin

  end;
    try
      FServidor :=ini.ReadString('Configuracao','Servidor','');
      FBase :=ini.ReadString('Configuracao','Base','');
      FPorta :=ini.ReadString('Configuracao','Porta','');
      FLogin :=ini.ReadString('Acesso','Login','');
      FSenha :=ini.ReadString('Acesso','Senha','');

    finally
      result := true;
      ini.free;
    end;

end;

end.
