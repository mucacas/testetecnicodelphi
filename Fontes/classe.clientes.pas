unit classe.clientes;

interface
uses
  FireDAC.Comp.Client, unit_funcoes, System.SysUtils;

type
  TClientes = Class

  private
    FUf: String;
    FCodigo: Integer;
    FNome: String;
    FCidade: String;
    FConexao: TFDConnection;

  public
    property Conexao : TFDConnection Read FConexao Write FConexao;
    property Codigo  : Integer Read FCodigo Write FCodigo;
    property Nome    : String Read FNome Write FNome;
    property Cidade  : String Read FCidade Write FCidade;
    property Uf      : String Read FUf Write FUf;

    Constructor Create (Conexao : TFDConnection);
    Destructor Destroy; Override;

    Function fnc_inserir_alterar(TipoOperacao : String; out Erro : String):boolean;
    Function fnc_consulta( texto : String ): TFDQuery;
    Function fnc_buscarNome(texto: String): TFDQuery;
    Procedure prc_deleta( id_chave : integer );

  End;
  var
    QryConsulta : TFDQuery;
implementation

{ TClientes }

constructor TClientes.Create(Conexao: TFDConnection);
begin
  FConexao := Conexao;

  QryConsulta := TFDQuery.Create(nil);
  QryConsulta.Connection := FConexao;
end;

destructor TClientes.Destroy;
begin
  QryConsulta.Destroy;
  inherited;
end;

Function TClientes.fnc_buscarNome(texto: String): TFDQuery;
begin
  try
    FConexao.Connected := False;
    FConexao.Connected := True;
    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add('Select clientes_nome From clientes Where clientes_codigo like :p_texto');
    QryConsulta.ParamByName('p_texto').AsString := texto;
    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;

end;
function TClientes.fnc_consulta(texto: String): TFDQuery;
begin
  try
    //FConexao.Connected := False;
    FConexao.Connected := True;

    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add('Select clientes_codigo, clientes_nome, clientes_cidade,clientes_uf');
    QryConsulta.SQL.Add('From clientes');
    QryConsulta.SQL.Add('Where clientes_nome like :p_texto');
    QryConsulta.ParamByName('p_texto').AsString := '%' + texto + '%';
    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;

end;

function TClientes.fnc_inserir_alterar(TipoOperacao: String; out Erro: String): boolean;
var
  QryInserir : TFDQuery;
begin
  Try
    Try
      FConexao.Connected := False;
      FConexao.Connected := True;

      QryInserir := TFDQuery.Create(nil);
      QryInserir.Connection := FConexao;

      QryInserir.Close;
      QryInserir.SQL.Clear;
      if TipoOperacao = 'INSERIR' then
      begin
        QryInserir.SQL.Add('Insert into clientes ( clientes_nome, clientes_cidade, clientes_uf)');
        QryInserir.SQL.Add(' values ( :p_clientes_nome, :p_clientes_cidade, :p_clientes_uf)');
      end
      else
      Begin
        QryInserir.SQL.Add('Update clientes set');
        QryInserir.SQL.Add(' clientes_nome   = :p_clientes_nome, ');
        QryInserir.SQL.Add(' clientes_cidade = :p_clientes_cidade, ');
        QryInserir.SQL.Add(' clientes_uf     = :p_clientes_uf');
        QryInserir.SQL.Add(' Where clientes_codigo = :p_clientes_codigo');
        QryInserir.ParamByName('p_clientes_codigo').AsInteger := FCodigo;
      End;

      QryInserir.ParamByName('p_clientes_nome').AsString   := FNome;
      QryInserir.ParamByName('p_clientes_cidade').AsString := FCidade;
      QryInserir.ParamByName('p_clientes_uf').AsString     := FUf;
      QryInserir.ExecSql;

      Result := True;

      fnc_consulta('');
    Except
      on E : Exception do
      begin
        Erro:= E.Message;
        Result := False;
      end;
    End;
  Finally
    QryInserir.Destroy;
  End;
end;

procedure TClientes.prc_deleta(id_chave: integer);
begin
  if fnc_criar_mensagem('Confirmação','Excluir Dados', 'Tem certeza que deseja EXCLUIR esse CLIENTE?','Confirma') then
  Begin
    FConexao.Connected := False;
    FConexao.Connected := True;
    FConexao.ExecSQL('Delete from clientes where clientes_codigo = :id_chave',[id_chave]);

    fnc_consulta('');
  End;
end;

end.
