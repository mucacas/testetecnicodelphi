object form_configurar_servidor: Tform_configurar_servidor
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 579
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object shp_fundo: TShape
    Left = 0
    Top = 0
    Width = 1005
    Height = 579
    Align = alClient
    Brush.Color = 15921906
    ExplicitLeft = 480
    ExplicitTop = 328
    ExplicitWidth = 65
    ExplicitHeight = 65
  end
  object pnl_fundo: TPanel
    Left = 8
    Top = 8
    Width = 989
    Height = 644
    BevelOuter = bvNone
    TabOrder = 0
    object pnl_topo: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 10
      Width = 983
      Height = 78
      Margins.Top = 10
      Margins.Bottom = 0
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      DesignSize = (
        983
        78)
      object lbl_titulo: TLabel
        Left = 87
        Top = 12
        Width = 252
        Height = 34
        AutoSize = False
        Caption = 'Configura'#231#227'o de Servidor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4006893
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_subtitulo: TLabel
        Left = 87
        Top = 43
        Width = 556
        Height = 25
        AutoSize = False
        Caption = 
          'Preencha de acordo com a localiza'#231#227'o e acesso ao seu Servidor de' +
          ' DADOS.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object img_icone: TImage
        Left = 16
        Top = 12
        Width = 57
        Height = 56
        Picture.Data = {
          0954506E67496D61676589504E470D0A1A0A0000000D49484452000000800000
          00800806000000C33E61CB0000000473424954080808087C0864880000000970
          485973000003B1000003B101F583ED490000001974455874536F667477617265
          007777772E696E6B73636170652E6F72679BEE3C1A00001C444944415478DAED
          5D07785455DA7E437A23A492104248219408040825202508D2214AB12C8A8022
          22AEAEA2A0AE7595455875655D57511175FDC115500860412014692194D04B12
          4249427AEFE5FFBE331398723399994C2693CCBCCFF33D77E6B63973CF7BCFF9
          CE395FB18269C09EC49BA413890F4947123FF9675F1277125B1217F9F9EEF2AD
          8B7C3F5FEF44524B52203F564852435246524E524D524452419245924E728B24
          93244DBEE5EFB92DFD308C092B23FE9607491F926E24A10A12803B156A0AA824
          C9204926B9A22067492E4346AA3683E62200BFCD43E4120159C5776EE93F6B00
          706BC2443845729CE420C969B46252188A00DC548F95CB50C8DE72734131493C
          C91E929DF2CFAD86104D21406F92992493217BCB8DD99D9832F2487E27D94CB2
          0D3282982C74ADB4AE24F320ABF81E862A84B58D0D3C3CBDE0E1E50DEF8E1DE1
          4E5B4F2F1F78F9F8887DEE9E9EB0B3B387A3B3B338BFBD5B07B175A2EF36B6B6
          E29883A3236A6B6B515C54288E951415A1A6A60615E5E5A8A828472D7D2E292E
          4259591972326F213B33137939D9B4BD859C2CFE9C83AC5BE9748E41EB8BBB8C
          1D24FF25898509B60CDA1080CFE1A6FD69924924D6FAFC1057725068378476EF
          892EC12108E81A8C40DA760A0814156D2A60C2A4DDB88EEB29C9B87E3519D748
          AE5EBE8C8BE7CE203F37A729B7BE4EF229C917908D384C028D11802BFE5D9281
          BADCB49DB53542C37AA0EFC041E8DD2F1261E17721A47B0FD8DADAB5F4FF6D12
          B26E65E0F2F9B3387BF204128FC72331E1180AF2741E359690AC26594992DFD2
          FFA9210204937C463246DB1B75EBD90B434644236A6434FA460E86B38B8BB697
          3619D55555B84415534ACDB793C2EFDA50ABE3E42CFBDEBE430738D3676E890C
          85BABA3A5C4DBA82F803FB7070EF6EC41FDC2FBA1E2DC1BAC25F49FEC3B732DA
          C3528114019E20799FC455D385FC96F71F1C85B193A6217AC224F8F8FAB5C81F
          E0B772C1AC6948B97C49ABF3EDED599770818BABABD0253CA9FB7127FDC3DBA7
          233CBC653A877F4017EAA282C47E5D50535D8D84C307F1FBF62DD8FDF336A167
          68011E39B05E75A3259E9F2201DA917C44B258D305FC60A6CF9E83C9331E3289
          BE7BF92B4BF0BF755F36CBBD5DDAB74717D255585F1931761C26DE3F135656DA
          E9CDAC74728BB0F1DB75D8F3EB0ED14A6900CF448E233963ECE7A7F86FBE2279
          ACA1137BF78FC482E75EC4B0D163D0AE5D3B6397B341FC65DE6CECF965BB517E
          6BE10BCB4896EA7C5DF6AD5BF8F6F34F88A85FA0ACB4B4A1D3589988264934CA
          9F91A39E000B21EB8BD4D039B02B5E7A7B8578034C113B36FF8057162F30CA6F
          75F4EB845F13CEEA7D3D0F3BFFBD7239367FF7B518B24AE02249248C3877C004
          E0C5169EF776543D38EBB1F978FEB5BF8931B6298349B073DB163107A0AA8415
          16E40B65ADA8A0002525C5A29FD617C161DDB139EE7093CB7BE2E8612C5D380F
          9919E9528759FF5A62AC67C704789DE42DD503CBDE5D8907E73E61AC72180D3C
          CE2F252214135178F22737270BB959994261CBCDCEC2ADF434DC484DC1B59464
          716E3D58795CB9661D468E1D2F79DF84437FE0CC8904DCD56F0006440D6BB41C
          DC2D3C316BAA94F2CAAB99BC6E6294568009708C6480E24EAE782680B983DF50
          9E10CAA7B13E572C770152F8EEF3FF60D51BAFDCFEFEE25BCBF1A7279E6AF4FE
          69D7AF617A7494945EC0D3EB46516C9800AC7C282DC7FE76FC5C8B0DEB5A23C6
          4474571AF2F1E8E8F79317B5BAF6AD17FE8C1FD77FABBAFB59C8268B9A1D4C00
          B5490856741A62BB05EA18D3AF8768D2EBE1DDD1173B4F9CD7EADA379F7F063F
          6DF8AFEA6EEE92DF3446D92509306BCE3CBCF2F7F78DF1FB6D02EBD7AEC1CAD7
          96096593E70996BDB3120FCC7DBCD1EB6EA45EC58CD143515E56A67AA86509C0
          58F2E6BB98BD609131CAD0267032FE08CE9E3C2E7485BE91831A3D9FF58B0533
          A7E16AD265A9C32D4F00C6F4D98F0922383A3919A32C66031E312C5D345FA9DB
          5081691080E1D739002FBDFD778C1A3751EB69500BA4C18AE2C72BFE862DDF7F
          27BA0B0D301D02D4A3579F082CF8CB8B1841E360539A0A6E0DE0E6FE9B4F3FC6
          0FDFAC559A5BD000D323403DB845E0AE61DA030F0B6DD70269F062D0A17D71D8
          F8ED57D8F7FBAFBACE40B62C01AC1D1C51535EA6F1426E0522060EC6D8293118
          3D61B265D808995D42C29183D819BB05BB76C48AB9FFC6D0C0B36E5902446D48
          C4A58F5E42CEA15FB4BE11CF93478D1C8D21C347A1DFA0216229B5AD83177478
          2AF7E81FFB7078EF1EB1FC5B5A52A2D5B5B6EDDD11F2E45B28CFBC89AB5FBFA7
          7AB86509101D27B354CA8DDF85ABEBDE43C1D9A33ADD945B87A06E61E8D37F20
          7A0F88448FF0DE08E9DE13F60E0EC6F84FCD86F41BD771F9FC399C4B3C89C484
          786116565C58A8D33DAC9D5CD0F9FE271130EB6922810752D6ADA067BC42F534
          D320403DF212E27063F367D422FC86BA5AFD0C5BD9822820300861BDC2D12528
          18014132430BFEECE9ED63328A25CFCBDFBC962A33084D9149CAE58BA2E28B0A
          0BF4BEAF43C7CEF09B3C07FED31E176F7F3D5A0501EA51919586B4EDDF2073F7
          66945ED3CE044B1B70E57B787909132C2F1F5F2284B7CC4C8B944C9959B8176C
          6CACE1DADE8D4A6C253307B7B1152D0A8BB5B58DB04164458B977C1985F9B2FF
          C02B7FD5B49FB5EFCA8A7261F69D959181BCDC6CB1FA97456371DEB2B089B806
          830DDDFF97BD033C078F85DF843FC183B656EDD40DAA5B15011451927C0E997B
          B7083DA1F8CA696A196A1BBDC61CC06FB7FB8051F01E3E059E43C79392A77912
          ADD512401155F9D9C83BBE17B9D455149C3E82D2EB97D964D618E56F71D838BB
          A27DAF81E8D07718DC23A3E11A16416FBAF6DD599B20802AAA0AF35078EE2849
          028A934EA3985A8BF2F45463FC9F6685B5A3339C837AC125245C54B45BF82038
          75EDA15385ABA24D12400AD525852849394FADC31594DD4842D9CD6494D296F5
          8AAA822679DC1814EDEC1C60E7E103C74E4170EC1C0C27FF10DA8650C5F78083
          6F60932A5B0A6643004DA8ABAE4225752395B9992419D4A5E4A0222703957999
          E273655E9638AFBAB840742D356525A8ABA9426D65396A48B1ABABA9464D6931
          AC4831E4B794D72C6C5CDCC435D64EAE42F9E209172B5B7B58936266EBEE0D7B
          0F5FDA7A5165779449074FD879FA52936EDCF90B0B01CC1C160298392C043073
          580860E6B010C0CC61218099C3420033878500660E0B01CC1C160298392C0430
          73580860E6B010C0CC61218099C3240930E297B4464D992C300C92FEF31AAE7D
          FF2FD5DD2D4B809ECB3E81EFF8875BFAD9B479D45655E2E89CC1284B4B513DD4
          B20460638AFEAB7F867370AF967E466D177575B8B07231D27FFE4EEA68CBFB06
          B2756BAFD7D7C22332BAC59E515B454D79292EAC5884CCB89F1A3AA5E509203B
          6A05DFB10F2074F172E1C56241D39177623F2E7DF0BCCC72BA61980801E4E0CA
          F7BF7F0102662E32BACD5C5B41E1B9785CFDF67D6DFD2D4D8B00F5E06E8195C3
          4E53E6C22920D418E56BD5A8ADAA40D6DEAD488BFD0AF9A70EEA72A96912E0CE
          555670BB6B307C46C5C07B640CECBD2C7102EAC1BE93F9270F88FE3D2B6E0BAA
          0AF5CA42D7B204E020911FBFF78E569EAF6C27DFBE6724DC078E26857194F86C
          656DB898FCAD01953919C83D1647B2477854B3A79436E0FC0A1C5370DBC6EF55
          0FB52C014EA6E58900464C826D9BBE6F2CD4B91284AB54F860B8F58AA4ED20B4
          A76D5BD21BD807921D630BCF1D43C199C3625B72F5824EF7E81AD20D8B5E7C19
          F74EBD0F9FBEBF82C4C4E2033001EAC1AED25FFEEB436C27226819DF46E517AC
          E0E0DB052EC1E142786EC1A9B3CCDB869D384C155CD115593785075349EA4594
          249D453149C9D5F3C231451F705695B94F3F87F1D3EE17EEF20C9327403D38EA
          76EC0F1B44A8F3A48BBA31BE21B0478EA37F10E9107EB073F7119E3A328F1D6F
          FA4E9F3DFD60DBC153B86B1912DC44B327126F2BB2D3E5DE4799C21B893F9767
          A452C5A70825AEA960F7F57BA7C460C6237325E307B61A0228828325ECDCF693
          08D1AE6DAA96A680BB157A6560EB2A4B1767EDE822F40C2606FBE0F3671B2717
          D45657DD7E3BAB8B640B5A3565C5C275ACDE8D4CB895556BDFA5E9030EAF7FF7
          E8B1183B651A468C19AF31CE62AB24802238DCE9E17D7B7068EF1E1C3DB0AF49
          51345A2BD81731ACD75D1832321A512338465294D6E1705A3D0114C14193922F
          5DC4A96347702A211EA74952539245C8B4B6044E36D5B34F0422382D5EFF81D4
          B40F94452FD1036D8A005260C531E9E2799178F1CA85F3484DBA8C6B292948BB
          714DA7D1454BC0CDDD43C43062E12057613DC3114A8A9C6F277F83FD469B2740
          43E0CA671264A6A78BD46F1CA38753B86667C93277E464668A983D79B9390627
          0A87B0E334711C7BC893B65EDEF2D471BEB298447CCC3FB02BDC3A347F567BB3
          25802EE011088327A6B89BE1404E555595A8AAAC447959A9C811CCB982388F30
          0790E2A1677D93EC4A956D65D54E2862B67676B2C052F6A613AECE4200338785
          00660E0B01CC1C160298192E9E3D8DB7963C8B0B67121B1B1EF3C1E3249C7BA6
          D9B2895A086044B0023B75D8003179A603D874A8075FDE1C65B210C088E020D3
          0F8FD7CBC6B23FC989E6289385004604670DF9E0EDD7F4B9F479920F9BA34C16
          021811CFCE79087B776A9F8341015B48629AA34C16023411A78E1DC53B4BFF22
          C2CA0FBFE75E3CFBEA1B220CBE2A3853D87373FFA4B658B6273A1A033CEE585C
          27E4E6227ACF1ED5CB79268C09B057A2086C9CC9BE651321D3173813FC216DCB
          6F21401370FCC8212C9E3D53294B08CF46CE7A741E162F7B4D6406FBE5A78DF8
          FEAB2F70E9BC7ADA79171B1B5C9B3A15360AD9D8AAE99AC0D85814494F7FF3DA
          FB5A9235246C9DB3542E8A539B1CEF7E0AC96E6DFE8385007A22FE8FFD78E6D1
          07A4B27E0A700E2591CEBEB8E124E0F7FAFA62E330F54CE333FEF803BF656468
          FAF922DCC9322E05260127A0DE83466021801E907AF3F5C1FF4545617227F564
          5B5B6FDEC4ECC3879B5A4C26C154925D9A4EB210404734F6E66B035F0707FC35
          3C1C8F76EDDAE039DF5CBD8A77CE9E45863E769877D0684B204980E3D7B36F1B
          2D5A70079A2A3FD8C505374B4B51A12173CA706F6FCC0F0EC6147AEB6DB5083B
          5F45F78A4D4BC397C9C9D89F95D5E079F6742F7F2727244B77371A492049806F
          627F439F01038DFE804D19ACED3FF9408C64E5F77777C796E1C3915D51812527
          4F6297424E6057520A1FEAD245547CCF26A4D23B5F582888B0FEDA352505F19E
          8E1DF18F880878D9DB63DAFEFD389E27D97A3309C692A8B9274912203CA23FBE
          DCB44D18375A20C3CC7B8609635855D457BE1B55743DF6D1DB7A8024C8D91953
          FCFD85B66F281457572396748414D23FEEA6166504493D0A881853890427A449
          C0EB097D557736E81A3678F828AC5AF395B07FB3001812D249EDEDE7667FEFE8
          D14A95DFD260128CD8B54B104405DC0AA8396268F40D0CE81A84D7577D8481C3
          86B7F4FF6A712C79620E7EDFBE55691FF7BD87C78E45081141175C2A2AC236EA
          DB8FE6E488A69D15BDB29A1A38534B31955A8CF7A949D7B7D5B8427A40D4CE9D
          52BAC8269219AA3B1B750E6593E7F131D3B1F085A5080C365F8F604E2439237A
          282A2A949D45B80FFEF1EEBB1BBD9E1F3237DDAB2F5F1615AF090B4343B1B26F
          DF46EF2985FB0E1C50D241E4E0A1446F922BAA07B4F60EE65101BB343D387701
          298891867AAEAD0A9FAC5A8E351FAE52DB1F3B6204462AF4C5AAE0B772714202
          0E666BE734DA9FEE1547F7D4157B49EF98B26F9FD4A1BF91BC2E75402FF7F0EE
          E1BD11F3D06C8C9934D5AC52C8B3893BAFE7DF4A4F53DABFB4674FBCDA4B3A9E
          D28F376E6011557E890EE9E3172D5A84D54B96A0FAD02154FDFC33EA48F7E026
          9DBB8E0052CC3BD8D9495EC7F3062B2FA8B9EDDD200923919CB89024002741CC
          3FF547A305AD4F21CFE9E3878EBA4764106FCB60CBE37B07F4529BDEFD2C3212
          0F0506AA9DBF96866DCFD3B0B056CB249AF634949B346912D6AE5D0B37379955
          731DE908A757AFC6D4152B904A8A9D23B5C41F0F18809901016AD7AF4F4DC593
          C78EA9EEE669639E3296F4F5970E14B9270F19BFAE47D2676F0AA7496DE1E3EB
          87C1234661C090A1E83B6010BA8676133A445BC1FFD67D89E5AF2C51DAC7E3FC
          8B1327AA296D9BE8CD9F7FF4688395DFA74F1FCC9C3913A36914111616061BBA
          BE4307E911D77DF7DD879F7E9205946A47CF9395CFE4C99385D2A8081E2276DF
          B1436A21E969924FA4EEAD3152684D7919D2B6AEC5B50DAB51997B0BBA828790
          BDFB0F4058AFDE226B78688F5E082252D898D0B049174C8F8E52F38C5E101222
          266214C14DF588DDBB512AD1EC87D0F9AB56AD424C4C8CD62FC7C08103714CE5
          CD4E1C3F1E5D9DD5DDEB79226A4D5292EA6E5E8ABC4BEADE5A858A65CF5A0E79
          92B6759D088AD01470E577EADC055D82824842D099869ADC72780AEF1C4FD229
          FC4426705303AFE7CF9F3E596DFF111A062ACEF0F1C39C448AD80189A9DB71E3
          C661FDFAF57077D7CDE3E8BDF7DEC3B265CB6E7FEF4DDDC3FE7BEE11AD812A78
          5839988681121805097B029D630573348CCC5D9B44E67043A69057047BEF0817
          2D1A627978CAD3C7DBDAC0C555D62F3ABBBAC2DABA1D1C1C9D60470A91F00872
          92BD0D3C5953597967A8C60618E56534CE2E2D4131BD99B2EFA5F0F2EE884716
          2E162D9214D880932D78B9E28F1D3C8084C307D58C3978266E878AB6CE4ADF9C
          2347D4EE3791BA89AD5BB752B9755F6361CFA7D5A4076CD9B205C1C1C1F82B29
          9DDE12BF71FBB7A409C895CA438438C8887092FF66938245730AF9ECC3BF22EF
          D81E9135DC1001158C890E1E9ED81C7748904D115CF12F3CFE68A3D6BBEB060F
          C6FD9D9597E4D99A87AD7A14D1AD5B37C4C7C7DF56EC9A0A560C4B9E7E5A8C0E
          A4B09948F8980682C8C1FDC47483450B677DA1E0F441418482334750782141E4
          F335752CFEE073F49D3803F9555628A8A9433E75DBEB1F198D9CF39A8D707949
          F7EC84094AAB7A374A4B11CEC336957359819B366D9A41CB5DF1DD77A8FCF147
          C963BC8AC8E5D0622939BED9C2C573B8B4D2AB1765E9E3457C9D33284939878A
          EC8C26DFDB9088F83016EEFD94A7BAE3467B8AF26B020FC554D7F359F962254C
          11FDFAF5C3F1E3C70D5EEE1A1A6296BEF45283C7BF4E49C1338DFF6E8DD1F305
          70AB509F3A9EB79C3E5EC4E929C811230D166E4D8C01DF710FA2E7CB9FAAEDDF
          334A7A38C643BDA15E5E788C1458294B9E870E1DC2F634E549A277DE7907AFBE
          FAAAE10B4FC3CBE2A79E429D86D9455E6F584744E019C8E20626A24C32610407
          53AE94A78FAFCCCBBE1D77AF3E7D7C757DDC9F8A723142A953880DC431831483
          4A593BB9C0DADE494424E328E8E2BBA30B9C024248A415402902C4D178BD2F8D
          D3AD350CDDEEDEB50B89F9CACFEE08F5C583060D8221919A9A8AF9F3E7E3F0FE
          FD184465FA37B5469D35C421AAA167768ACA356AB7BA9DA84912A0A5214580C2
          E9D31BBD2E6CFB76B57E378DDE423F3F3F83966FCC9831D8B5EB8EA95FB48F8F
          B049680CED376D52DB67218004F42580172965952ACBB0BC7A68D7C0DCBD3EE0
          21A113BDED959595B7F739D0D03233A671BF110B01B484BE04E8462DC02D9516
          203D3D1DBEBE865B30E316C5DF5F394611EB26695A8C322C04D0125204600F9E
          BEEEEE4A4E1CAA30860EC0F71B326488D23E364839316E5C83D7B0B3C9A9BC3C
          298F230B01A4D0D828600E8D02A6B4D02880EFF7DA6BCA0EA693A82CEBA3A2D4
          CE658BE2AF5BE328A0A561C87980888808310F608855517635E3798553A74E29
          EDFF80F63D1EACEC8F68B2F300AD01090B47A3F082E687273913585686F01D3B
          D41EE8E6CD9BC5926E53B171E346B184AC08AEC0731327C25FC1825B8799C0A3
          16024880672ECFBCF128CA6E24693C4F6A2D604C5C9C9ACD5F6868A858CE6DCA
          5A403EE916919191485259EA1DE2E989DF468D52DAA7E55A007B121B6E2DA0AD
          81C3C597249D41DEC903C83FB95FA47C1113510A905A0D8CCBCC14B6F9AA9840
          AD456C6CACDEAB8193274FC62FBFA8C716D84EBF3F5CC51E518BD54016EE479A
          B61A684E6013B913CF4E52DB7F68CC1884ABBCD931070E60B7BA65AEB007D8B0
          614383963F52282C2CC4238F3C2296925521E55D7C416E0F2061873412320228
          C142001D7074DE50B104AE08298BA0CB728B20294350EE0ED8C08375024D8A21
          2B7CAC3B2C5DBA54ADD967F088848D42547D125E3871029F2727AB9ECE85668B
          20B5BAB6104007DCFCF1735CFAE845A57D5C116C13E8AA62E6C68621731BB109
          9C31638698D60D0C0C8427F5E539A43BF03CFF4E7A83376DDA84C444E9E8706C
          09F435E91FD354268498706186B409B44019D525453838B3A79A9D83A1AC82B5
          0157FE07D4E2CC0B560F436338AB600B0124C1AB8F8767F7174BD88AD0E417B0
          E5E64D3C459552AC835F4043E0D6E65322DB547FE970F5FAFA05B0FD929295E2
          D08DE7451E1F0B9491B27639AE7EB3526DFF36D2C44768F00C4A2A2EC69F8F1F
          D7E8E3DF18F8FEABFBF7170EA90D415FCFA07812255F2FFF98C711F6DC3F9AF5
          61B636B0F1CAD1B94385FD8122B4F50D64EC484FC73F2F5EC411EAEBB5E914B8
          72867879E1B9B0304CD07249591FDF409E587E5BF540B7C52BD079C6C2667FB0
          AD05675E7F1459FBD4BD830F8D1D8B501DCDD8D3CACA441028AEA86BA5A5C8A0
          EF79A4B8B99322E9EBE8882E4E4E82583CCCEBA4638C066E6D86E8E81DDC9184
          C70D6A26259DA6CE45E8A27761EDD0B0B589B960DF783F3553350E00B18F8662
          A6161F60240D4125C2C534181F80F10464B1E7D4C0491FBB3DB3025E4327884C
          1CE68AF8F9C3C414B12AFAB9BB63AB4A8410EE8BF767668A317A734508E1377D
          B88F8F92573257BE8630313CF317A1BA53B146BF2099DFD00FBB76EF87AE8FBE
          04CFA871225FB0B9814DDD4F2D89913458AD27415645055E948811F46040001E
          0F0969728CA02F9292B0E1FA75B51841AB6858E86D6FAF293C8CC618418A9F39
          20F1B39A0AC22D42A7298FC177DCC36697353CEFC47E9C7E79962409B83B4893
          BB7137045E3B785C8F28615F24274BBA9AD5837511D61552A4E316361A254C15
          DC0A30115C35158E5B01B7DE43E0CD29E4874F319B61A32612680B5E4AE67903
          362C69086CCEBDFCDC39A3C4099442571236986FD8CE4805CE413DE111190DF7
          01A3881851B274AF6D14862001E3BBA82849CB2203460A6D345C6C635ADD6892
          E5248375F9656E1D9C02BBA37DAF81702371E9D61BCE813D84CD7E5B416324B0
          75F32AAF2E29609F8506FFB4A9C40AD6E69C7B205B50E028D47A8510B56A670D
          47FF6059FAF88050F199C5C93F08B6EE3EAD52B1942201FDCF2A6B47A77F5697
          14BD415F7968309B641149B8EAF5AC20A64E99A24BB4701E86F0A2CE7F49F884
          974978754A355A38AF5BC769F31F741DD775219947328BA4A7A11E2493C3B683
          9748196FE7E14B5B6F592A7979FA78FE2E3284BBC8D6DD6D9C64D9C4AD1D1CD1
          CED61E56B676B7E72AEABD87D873A85ABE68535B51263C977931A72A2F0B5585
          B9C215ADAA304FF6393F0735A545D46A852168DE5FB5516E7922EF22C9A1EB9B
          D7DC4AFEECF5FB6A2BCA7982FE1779A55C91B886D7E339CC8792318001F305D0
          385D58F93C051DF305E80B5E5F66227053C331CD5ADF2B2C01D7B00844AE8953
          DC95434FE90A5579A2559D5522DAD52692729E1817EDAECF8A1967FE98AAC775
          3C056958F762390C35B3C3B31163E43294A4BB01EF6D74F4793FF659CF7E238E
          D839E0F2AF43DD729B7EC7DBE0DC3FEFEB795DB3E50C6A0E7892B0A13A2B8FDC
          3AF04244D766FA2D4383952B1ED336878B3267FF4AD0F3BA66CB1A662C7007CE
          44E0B56976CB0D956F5993F534623934816752E692FCD04CF7E76E92E3EA84E8
          704DB3E70D3405B0F7A40F096B5F1DE59FFD143E7BC9CB5AAF40F19C2A8F4658
          F3B3975F5FBFD0C17D332B69EC3D593F35C6C3A66AF9966DB6B3E4DB6CF9B6FE
          F319C8B4E8E604B7883CEDDE0F9A4754EC99C2AD05670E3DDD5C85F97F84EDCA
          15B6B1EDEF0000000049454E44AE426082}
        Proportional = True
      end
      object pnl_botoes: TPanel
        Left = 736
        Top = 19
        Width = 226
        Height = 40
        Anchors = [akTop, akRight]
        BevelOuter = bvNone
        TabOrder = 0
        object pnl_btn_confirmar: TPanel
          Left = 0
          Top = 0
          Width = 110
          Height = 40
          Align = alLeft
          BevelOuter = bvNone
          Color = 4006893
          ParentBackground = False
          TabOrder = 0
          object btn_confirmar: TSpeedButton
            Left = 0
            Top = 0
            Width = 110
            Height = 40
            Cursor = crHandPoint
            Align = alClient
            Caption = 'Confirmar'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_confirmarClick
            ExplicitTop = 5
            ExplicitHeight = 50
          end
        end
        object pnl_cancelar: TPanel
          Left = 116
          Top = 0
          Width = 110
          Height = 40
          Align = alRight
          BevelOuter = bvNone
          Color = 15921906
          ParentBackground = False
          TabOrder = 1
          object btn_cancelar: TSpeedButton
            Left = 0
            Top = 0
            Width = 110
            Height = 40
            Cursor = crHandPoint
            Align = alClient
            Caption = 'Cancelar'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_cancelarClick
            ExplicitHeight = 52
          end
        end
      end
    end
    object pnl_config_nova: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 98
      Width = 983
      Height = 213
      Margins.Top = 10
      Margins.Bottom = 0
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object lbl_titulo_config_nova: TLabel
        Left = 16
        Top = 12
        Width = 297
        Height = 29
        AutoSize = False
        Caption = 'Nova Configura'#231#227'o de Servidor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4006893
        Font.Height = -16
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 32
        Top = 58
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Caminho do Servidor *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 32
        Top = 128
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Nome da Base de DADOS *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 356
        Top = 128
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'LOGIN *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 681
        Top = 128
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'SENHA *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 681
        Top = 58
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Porta *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object pnl_linha_config_nova: TPanel
        Left = 16
        Top = 35
        Width = 946
        Height = 2
        BevelOuter = bvNone
        Color = clSilver
        ParentBackground = False
        TabOrder = 0
      end
      object Panel1: TPanel
        AlignWithMargins = True
        Left = 32
        Top = 80
        Width = 584
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 1
        object edt_caminho: TEdit
          Tag = 5
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 574
          Height = 23
          Hint = 'Caminho do Banco'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel2: TPanel
        AlignWithMargins = True
        Left = 32
        Top = 150
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 3
        object edt_nome_base: TEdit
          Tag = 5
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          Hint = 'Nome da Base de Dados'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel3: TPanel
        AlignWithMargins = True
        Left = 356
        Top = 150
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 4
        object edt_login: TEdit
          Tag = 5
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          Hint = 'Login'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel4: TPanel
        AlignWithMargins = True
        Left = 681
        Top = 150
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 5
        object edt_senha: TEdit
          Tag = 5
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          Hint = 'Senha'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          PasswordChar = '*'
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel5: TPanel
        AlignWithMargins = True
        Left = 681
        Top = 80
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 2
        object edt_porta: TEdit
          Tag = 5
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          Hint = 'Porta de Conex'#227'o'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
        end
      end
    end
    object pnl_config_atual: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 321
      Width = 983
      Height = 237
      Margins.Top = 10
      Margins.Bottom = 0
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      Enabled = False
      ParentBackground = False
      TabOrder = 2
      TabStop = True
      object lbl_titulo_config_atual: TLabel
        Left = 16
        Top = 12
        Width = 252
        Height = 34
        AutoSize = False
        Caption = 'Configura'#231#227'o Atual'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4006893
        Font.Height = -16
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 40
        Top = 66
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Caminho do Servidor *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 40
        Top = 136
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Nome da Base de DADOS *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 364
        Top = 136
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'LOGIN *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 689
        Top = 66
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'Porta *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 689
        Top = 136
        Width = 260
        Height = 21
        AutoSize = False
        Caption = 'SENHA *'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object pnl_linha_config_atual: TPanel
        Left = 16
        Top = 35
        Width = 946
        Height = 2
        BevelOuter = bvNone
        Color = clSilver
        ParentBackground = False
        TabOrder = 0
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 40
        Top = 88
        Width = 584
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15856113
        ParentBackground = False
        TabOrder = 1
        object edt_caminho_atual: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 574
          Height = 23
          TabStop = False
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 15856113
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel7: TPanel
        AlignWithMargins = True
        Left = 40
        Top = 158
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15856113
        ParentBackground = False
        TabOrder = 2
        object edt_nome_base_atual: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          TabStop = False
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 15856113
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel8: TPanel
        AlignWithMargins = True
        Left = 364
        Top = 158
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15856113
        ParentBackground = False
        TabOrder = 3
        object edt_login_atual: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          TabStop = False
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 15856113
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel9: TPanel
        AlignWithMargins = True
        Left = 689
        Top = 88
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15856113
        ParentBackground = False
        TabOrder = 4
        object edt_porta_atual: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          TabStop = False
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 15856113
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
        end
      end
      object Panel10: TPanel
        AlignWithMargins = True
        Left = 689
        Top = 158
        Width = 260
        Height = 33
        Margins.Left = 20
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15856113
        ParentBackground = False
        TabOrder = 5
        object edt_senha_atual: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 250
          Height = 23
          TabStop = False
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = 15856113
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -1
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
        end
      end
    end
  end
end
