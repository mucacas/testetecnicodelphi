unit unit_funcoes;

interface
  uses
  vcl.Controls, vcl.Forms, vcl.Dialogs, vcl.StdCtrls, vcl.Mask,
  System.SysUtils, FireDAC.Comp.Client;

  function fnc_criar_mensagem(TituloJanela, TituloMsg, Msg, Tipo: string): Boolean;
  function fnc_proximo_codigo(Tabela, Campo:String):integer;
  function Ret_Numero(Key: Char; Texto: string; EhDecimal: Boolean = False): Char;
  function minhaData(Data: TDate): String;
  procedure prc_validar_campos_obrigatorios(Form : TForm);

implementation
  uses unit_mensagens, unit_dados;



function fnc_criar_mensagem(TituloJanela, TituloMsg, Msg, Tipo: string): Boolean;
  begin
    result := False;
    form_mensagens               := Tform_mensagens.Create(nil);
    form_mensagens.sTituloJanela := TituloJanela;
    form_mensagens.sTituloMsg    := TituloMsg;
    form_mensagens.sMsg          := Msg;
    form_mensagens.sTipo         := Tipo;

    form_mensagens.ShowModal;
    result := form_mensagens.bRespostaMSG;
  end;

  procedure prc_validar_campos_obrigatorios(Form : TForm);
  var
    i : integer;
  begin
    for i  := 0 to form.ComponentCount - 1 do
      begin
        if form.Components[i].Tag = 5 then
        //tEdit
        begin
          if form.Components[i] is tEdit  then
            if( (form.Components[i] as tEdit).hint <> '') and ((form.Components[i] as tEdit).Text = '') then
            begin
              fnc_criar_mensagem( 'ATEN��O',
                                  'CAMPO N�O PREENCHIDO',
                                  'Faltou Preencher o Campo (' + ((form.Components[i] as tEdit).hint)+')',
                                  'OK' );
              Abort;
            end;
        end;
      end;
  end;

  function minhaData(Data: TDate): String;
  begin
     Result := QuotedStr(FormatDateTime('yyyy-mm-dd', Data));
  end;


  function fnc_proximo_codigo(Tabela, Campo:String):integer;
  var
    QryConsulta : TFDQuery;
  begin
    result := 1;
    try
      form_dados.FDConnection.Connected := False;
      form_dados.FDConnection.Connected := True;

      QryConsulta := TFDQuery.Create(nil);
      QryConsulta.Connection := form_Dados.FDConnection;

      QryConsulta.close;
      QryConsulta.sql.Clear;
      QryConsulta.sql.add('Select MAX(' + Campo + ') from ' + tabela );
      showMessage(QryConsulta.sql.text);
      if QryConsulta.FieldByName( Campo ).AsString <> '' then
        result := QryConsulta.FieldByName( Campo ).AsInteger + 1;
    finally
    QryConsulta.Destroy;
    end;
  end;

function  Ret_Numero(Key: Char; Texto: string; EhDecimal: Boolean = False): Char;
  begin
    if  not EhDecimal then
      begin
        if  not ( Key in ['0'..'9', Chr(8)] ) then
            Key := #0
      end
    else
      begin
        if  Key = #46 then
            Key := FormatSettings.DecimalSeparator;
        if  not ( Key in ['0'..'9', Chr(8), FormatSettings.DecimalSeparator] ) then
            Key := #0
        else
          if  ( Key = FormatSettings.DecimalSeparator ) and ( Pos( Key, Texto ) > 0 ) then
              Key := #0;
      end;
    Result := Key;
  end;
end.
