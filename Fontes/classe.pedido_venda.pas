﻿unit classe.pedido_venda;

interface
uses
  FireDAC.Comp.Client, unit_funcoes, System.SysUtils, Vcl.Dialogs, Data.DB;

type
  TPedido_Venda = Class
  private
    FProdutos_Codigo: Integer;
    FAutoincremento: Integer;
    FNumero_Pedido: Integer;
    FTransacao: Integer;
    FConexao: TFDConnection;
    FProdutos_Quantidade: Double;
    FProdutos_Valor_Unitario: Double;
    FProdutos_Valor_Total: Double;
    FProdutos_Descricao: String;
    FPedido_Valor_Total: Double;
    FPedido_Data_Emissao: TDate;
    FPedido_Cliente_Codigo: Integer;


  public
    property Conexao                 : TFDConnection Read FConexao Write FConexao;
    property Numero_Pedido           : Integer Read FNumero_Pedido Write FNumero_Pedido;
    property Autoincremento          : Integer Read FAutoincremento Write FAutoincremento;
    property Produtos_Codigo         : Integer Read FProdutos_Codigo Write FProdutos_Codigo;
    property Transacao               : Integer Read FTransacao Write FTransacao;
    property Produtos_Quantidade     : Double Read FProdutos_Quantidade Write FProdutos_Quantidade;
    property Produtos_Valor_Unitario : Double Read FProdutos_Valor_Unitario Write FProdutos_Valor_Unitario;
    property Produtos_Valor_Total    : Double Read FProdutos_Valor_Total Write FProdutos_Valor_Total;
    property Produtos_Descricao      : String Read FProdutos_Descricao Write FProdutos_Descricao;
    property Pedido_Cliente_Codigo   : Integer Read FPedido_Cliente_Codigo Write FPedido_Cliente_Codigo;
    property Pedido_Data_Emissao     : TDate Read FPedido_Data_Emissao Write FPedido_Data_Emissao;
    property Pedido_Valor_Total      : Double Read FPedido_Valor_Total Write FPedido_Valor_Total;

    Constructor Create (Conexao : TFDConnection);
    Destructor Destroy; Override;

    Function fnc_inserir_alterar(TipoOperacao : String; out Erro : String):boolean;
    function fnc_inserir_alterar_Pedido(TipoOperacao: String; out Erro : String):boolean;
    Function fnc_consulta( texto : String ): TFDQuery;
    function fnc_consulta_pedidos(texto: String; contemWhere : boolean): TFDQuery;
    function fnc_get_numero_pedido : integer;
    Procedure prc_deleta( id_chave : integer );
  End;

  var
    QryConsulta : TFDQuery;
implementation

{ TPedido_Venda }

constructor TPedido_Venda.Create(Conexao: TFDConnection);
begin
  FConexao := Conexao;

  QryConsulta := TFDQuery.Create(nil);
  QryConsulta.Connection := FConexao;
end;

destructor TPedido_Venda.Destroy;
begin
  QryConsulta.Destroy;
  inherited;
end;

function TPedido_Venda.fnc_consulta(texto: String): TFDQuery;
begin
  try
    FConexao.Connected := False;
    FConexao.Connected := True;

    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add(' SELECT  b.produtos_produtos_codigo as codigo, c.produtos_descricao as descricao, ');
    QryConsulta.SQL.Add(' b.produtos_do_pedido_quantidade as quantidade, ');
    QryConsulta.SQL.Add(' b.produtos_do_pedido_valor_unitario as valor_unitario, ');
    QryConsulta.SQL.Add(' b.produtos_do_pedido_valor_total as valor_total  ');
    QryConsulta.SQL.Add(' FROM pedidos_dados_gerais a ');
    QryConsulta.SQL.Add(' LEFT JOIN produtos_do_pedido b on ');
    QryConsulta.SQL.Add(' b.pedidos_dados_gerais_nmr_pedido = a.pedidos_dados_gerais_nmr_pedido ');
    QryConsulta.SQL.Add(' LEFT JOIN produtos c on c.produtos_codigo = b.produtos_produtos_codigo ');
    QryConsulta.SQL.Add(' Where a.pedidos_dados_gerais_nmr_pedido like :p_texto');
    QryConsulta.ParamByName('p_texto').AsString := texto ;
    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;
end;

function TPedido_Venda.fnc_consulta_pedidos(texto: String; contemWhere : boolean): TFDQuery;
begin
  try
    FConexao.Connected := False;
    FConexao.Connected := True;

    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add(' SELECT a.pedidos_dados_gerais_nmr_pedido as numero_pedido,');
    QryConsulta.SQL.Add(' c.clientes_nome as cliente_nome,');
    QryConsulta.SQL.Add(' a.pedidos_dados_gerais_data_emissao as data_emissao,');
    QryConsulta.SQL.Add(' a.pedidos_dados_gerais_valor_total as valor_total');
    QryConsulta.SQL.Add(' FROM pedidos_dados_gerais a');
    QryConsulta.SQL.Add(' LEFT JOIN clientes c on c.clientes_codigo = a.cli_clientes_codigo');
    if contemWhere = true then
    begin
      QryConsulta.SQL.Add(' Where a.pedidos_dados_gerais_nmr_pedido like :p_texto');
      QryConsulta.ParamByName('p_texto').AsString := texto;
    end;

    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;
end;

function TPedido_Venda.fnc_get_numero_pedido: integer;
var
  numero_pedido: integer;
begin
  try
    FConexao.Connected := False;
    FConexao.Connected := True;
    QryConsulta.Close;
    QryConsulta.sql.Clear;
    QryConsulta.sql.add('Select pedidos_dados_gerais_nmr_pedido from produtos_do_pedido');
    QryConsulta.sql.add('ORDER BY pedidos_dados_gerais_nmr_pedido desc limit 1');

    numero_pedido := QryConsulta.FieldByName('pedidos_dados_gerais_nmr_pedido').AsInteger;
    QryConsulta.Open;
  finally
    Result := numero_pedido;
  end;

end;

function TPedido_Venda.fnc_inserir_alterar(TipoOperacao: String;
  out Erro: String): boolean;
var
  QryInserir : TFDQuery;
begin
  Try
    Try
      FConexao.Connected := False;
      FConexao.Connected := True;

      QryInserir := TFDQuery.Create(nil);
      QryInserir.Connection := FConexao;

      QryInserir.Close;
      QryInserir.SQL.Clear;
      if TipoOperacao = 'INSERIR' then
      begin
        {
        QryInserir.SQL.Add('INSERT INTO pedidos_dados_gerais (');
        QryInserir.SQL.Add(' transacao, ');
        QryInserir.SQL.Add(' cli_clientes_codigo, ');
        QryInserir.SQL.Add(' pedidos_dados_gerais_data_emissao )');
        QryInserir.SQL.Add('VALUES (');
        QryInserir.SQL.Add(' :p_transacao, ');
        QryInserir.SQL.Add(' :p_cli_clientes_codigo, ');
        QryInserir.SQL.Add(' :p_pedidos_dados_gerais_data_emissao);');
        }

        //QryInserir.SQL.Add(' SET @id = LAST_INSERT_ID(); ');

        QryInserir.SQL.Add('INSERT INTO produtos_do_pedido (');
        QryInserir.SQL.Add(' pedidos_dados_gerais_nmr_pedido, ');
        QryInserir.SQL.Add(' produtos_produtos_codigo, ');
        QryInserir.SQL.Add(' transacao, ');
        QryInserir.SQL.Add(' produtos_do_pedido_quantidade, ');
        QryInserir.SQL.Add(' produtos_do_pedido_valor_unitario, ');
        QryInserir.SQL.Add(' produtos_do_pedido_valor_total) ');
        QryInserir.SQL.Add('VALUES( ');
        QryInserir.SQL.Add(' :p_numero_pedido,');
        //QryInserir.SQL.Add(' :p_produtos_produtos_codigo, ');
        QryInserir.SQL.Add(' :p_produtos_produtos_codigo, ');
        QryInserir.SQL.Add(' :p_transacao, ');
        QryInserir.SQL.Add(' :p_produtos_do_pedido_quantidade, ');
        QryInserir.SQL.Add(' :p_produtos_do_pedido_valor_unitario, ');
        QryInserir.SQL.Add(' :p_produtos_do_pedido_valor_total) ');

      end
      else
      Begin
        QryInserir.SQL.Add(' UPDATE produtos_do_pedido ');
        QryInserir.SQL.Add(' SET  ');
        QryInserir.SQL.Add(' produtos_do_pedido_quantidade = :p_produtos_do_pedido_quantidade, ');
        QryInserir.SQL.Add(' produtos_do_pedido_valor_unitario = :p_produtos_do_pedido_valor_unitario, ');
        QryInserir.SQL.Add(' WHERE produtos_do_pedido_autoincrem = p_autoincremeto ');
        QryInserir.ParamByName('p_autoincremento').AsInteger := FAutoincremento;
      End;
      QryInserir.ParamByName('p_numero_pedido').AsInteger                   := FNumero_Pedido;
      QryInserir.ParamByName('p_produtos_produtos_codigo').AsInteger        := FProdutos_Codigo;
      QryInserir.ParamByName('p_transacao').AsInteger                       := FTransacao;
      QryInserir.ParamByName('p_produtos_do_pedido_quantidade').AsFloat     := FProdutos_Quantidade;
      QryInserir.ParamByName('p_produtos_do_pedido_valor_unitario').AsFloat := FProdutos_Valor_Unitario;
      QryInserir.ParamByName('p_produtos_do_pedido_valor_total').AsFloat    := FProdutos_Valor_Total;
      //ShowMessage(QryInserir.sql.Text);
      QryInserir.ExecSql;

      Result := True;

      fnc_consulta('');

    Except
      on E : Exception do
      begin
        Erro:= E.Message;
        Result := False;
      end;
    End;
  Finally
    QryInserir.Destroy;
  End;
end;

function TPedido_Venda.fnc_inserir_alterar_Pedido(TipoOperacao: String;
  out Erro: String): boolean;
var
  QryInserir : TFDQuery;
begin
  Try
    Try
      FConexao.Connected := False;
      FConexao.Connected := True;

      QryInserir := TFDQuery.Create(nil);
      QryInserir.Connection := FConexao;

      QryInserir.Close;
      QryInserir.SQL.Clear;
      if TipoOperacao = 'INSERIR' then
      begin

        QryInserir.SQL.Add('INSERT INTO pedidos_dados_gerais (');
        QryInserir.SQL.Add(' transacao, ');
        QryInserir.SQL.Add(' cli_clientes_codigo, ');
        QryInserir.SQL.Add(' pedidos_dados_gerais_data_emissao )');
        QryInserir.SQL.Add('VALUES (');
        QryInserir.SQL.Add(' :p_transacao, ');
        QryInserir.SQL.Add(' :p_cli_clientes_codigo, ');
        QryInserir.SQL.Add(' :p_pedidos_dados_gerais_data_emissao);');

      end
      else
      Begin
        QryInserir.SQL.Add(' UPDATE pedidos_dados_gerais ');
        QryInserir.SQL.Add(' SET  ');
        QryInserir.SQL.Add(' pedidos_dados_gerais_valor_total = :p_pedidos_dados_gerais_valor_total, ');
        QryInserir.SQL.Add(' WHERE produtos_do_pedido_autoincrem = p_numero_pedido ');
        QryInserir.SQL.Add(' and transacao = p_transacao ');
        QryInserir.ParamByName('p_numero_pedido').AsInteger := FNumero_Pedido;
        QryInserir.ParamByName('p_transacao').AsInteger     := FTransacao;
      End;

      QryInserir.ParamByName('p_transacao').AsInteger                          := FTransacao;
      QryInserir.ParamByName('p_cli_clientes_codigo').AsInteger                := FPedido_Cliente_Codigo;
      QryInserir.ParamByName('p_pedidos_dados_gerais_data_emissao').AsDateTime := FPedido_Data_Emissao;
      QryInserir.ParamByName('p_pedidos_dados_gerais_valor_total').AsFloat     := FPedido_Valor_total;
      QryInserir.ParamByName('p_produtos_produtos_codigo').asInteger           := FProdutos_Codigo;

      QryInserir.ExecSql;

      Result := True;

      fnc_consulta('');

    Except
      on E : Exception do
      begin
        Erro:= E.Message;
        Result := False;
      end;
    End;
  Finally
    QryInserir.Destroy;
  End;
end;

procedure TPedido_Venda.prc_deleta(id_chave: integer);
begin
  if fnc_criar_mensagem('Confirmação','Excluir Dados', 'Tem certeza que deseja EXCLUIR esse Pedido de Venda?','Confirma') then
  Begin
    FConexao.Connected := False;
    FConexao.Connected := True;
    FConexao.ExecSQL('Delete from pedidos_dados_gerais where pedidos_dados_gerais_nmr_pedido = :id_chave',[id_chave]);

    fnc_consulta('');
  End;
end;

end.
