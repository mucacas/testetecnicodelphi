unit unit_pedido_venda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls,
  Data.DB, Vcl.Grids, Vcl.DBGrids, unit_dados, classe.clientes,classe.produtos, Vcl.DBCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  classe.pedido_venda, Vcl.Dialogs, System.SysUtils, Datasnap.DBClient,
  Datasnap.Provider;


type
  Tform_pedido_venda = class(TForm)
    pnl_main: TPanel;
    lbl_titulo_main: TLabel;
    lbl_total: TLabel;
    pnl_linha: TPanel;
    pnl_total: TPanel;
    edt_total: TEdit;
    pnl_btn_gravar_pedido: TPanel;
    btn_gravar_pedido: TSpeedButton;
    pnl_btn_cancelar: TPanel;
    dbg_registros: TDBGrid;
    btn_cancelar: TSpeedButton;
    pnl_topo: TPanel;
    pnl_formulario: TPanel;
    lbl_cliente_codigo: TLabel;
    lbl_quantidade: TLabel;
    lbl_valor_unitario: TLabel;
    pnl_btn_inserir: TPanel;
    btn_inserir: TSpeedButton;
    pnl_clientes: TPanel;
    pnl_quantidade: TPanel;
    edt_quantidade: TEdit;
    pnl_valor_unitario: TPanel;
    edt_valor_unitario: TEdit;
    pnl_btn_incluir_clientes: TPanel;
    btn_incluir_clientes: TSpeedButton;
    Ds_Clientes: TDataSource;
    lbl_produto: TLabel;
    pnl_produtos: TPanel;
    pnl_btn_incluir_produtos: TPanel;
    btn_incluir_produtos: TSpeedButton;
    Ds_Produtos: TDataSource;
    lbl_ajuda: TLabel;
    Ds_Pedido_Venda: TDataSource;
    edt_codigo_produto: TEdit;
    pnl_edt_produto_descricao: TPanel;
    edt_produto_descricao: TEdit;
    lbl_produto_descricao: TLabel;
    pnl_edt_nome_cliente: TPanel;
    edt_nome_cliente: TEdit;
    pnl_edt_codigo_cliente: TPanel;
    edt_codigo_cliente: TEdit;
    lbl_cliente_descricao: TLabel;
    btn_consulta_cliente: TSpeedButton;
    btn_consulta_produto: TSpeedButton;
    pnl_btn_excluir_pedido: TPanel;
    btn_excluir_pedido: TSpeedButton;
    pnl_btn_consultar_pedido: TPanel;
    btn_consultar_pedido: TSpeedButton;
    Ds_Produtos_Venda: TDataSource;
    lbl_numero_pedido: TLabel;
    FDQuery1: TFDQuery;
    FDQuery1codigo: TIntegerField;
    FDQuery1descricao: TStringField;
    FDQuery1quantidade: TBCDField;
    FDQuery1valor_unitario: TBCDField;
    FDQuery1valor_total: TBCDField;
    procedure btn_cancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_incluir_clientesClick(Sender: TObject);
    procedure btn_incluir_produtosClick(Sender: TObject);
    procedure btn_inserirClick(Sender: TObject);
    procedure edt_quantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure btn_consulta_clienteClick(Sender: TObject);
    procedure btn_consulta_produtoClick(Sender: TObject);
    procedure edt_codigo_clienteExit(Sender: TObject);
    procedure edt_codigo_clienteKeyPress(Sender: TObject; var Key: Char);
    procedure edt_codigo_produtoKeyPress(Sender: TObject; var Key: Char);
    procedure btn_consultar_pedidoClick(Sender: TObject);
    procedure btn_excluir_pedidoClick(Sender: TObject);
    procedure btn_gravar_pedidoClick(Sender: TObject);
    procedure edt_valor_unitarioKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }

  public
    { Public declarations }
    Numero_pedido : Integer;
    _Transacao : integer;
  end;

var
  form_pedido_venda: Tform_pedido_venda;
implementation

{$R *.dfm}

uses unit_principal, unit_clientes, unit_produtos, unit_funcoes, unit_consulta_vendas;


procedure Tform_pedido_venda.btn_cancelarClick(Sender: TObject);
begin
  form_principal.pnl_menu.enabled := true;
  close;
end;

procedure Tform_pedido_venda.btn_consultar_pedidoClick(Sender: TObject);
begin
  try
    form_consulta_vendas := tForm_Consulta_Vendas.Create(Self);
    form_consulta_vendas.ShowModal;
  finally
    Form_produtos.Free;
  end;
end;

procedure Tform_pedido_venda.btn_consulta_clienteClick(Sender: TObject);
begin
  if edt_codigo_cliente.text <> '' then
  begin
    Ds_Clientes.DataSet   := form_dados.Clientes.fnc_buscarNome(edt_codigo_cliente.Text);
    edt_nome_cliente.Text := Ds_Clientes.DataSet.FieldByName('clientes_nome').AsString;
  end
  else
  begin
    fnc_criar_mensagem( 'ATEN��O',
                        'PREENCHIMENTO DE CAMPO',
                        'Para realizar a busca � necess�rio preencher o c�digo do cliente!',
                        'OK' );
    edt_codigo_cliente.SetFocus;
    Exit;
  end;
end;

procedure Tform_pedido_venda.btn_consulta_produtoClick(Sender: TObject);
begin
  if edt_codigo_produto.Text <> '' then
  begin
    ds_produtos.DataSet        := form_dados.Produtos.fnc_buscarProduto(edt_codigo_produto.Text);
    edt_produto_descricao.Text := ds_produtos.DataSet.FieldByName('produtos_descricao').AsString;
    edt_valor_unitario.Text    := ds_produtos.DataSet.FieldByName('produtos_preco_venda').AsString;
  end
  else
  begin
    fnc_criar_mensagem( 'ATEN��O',
                        'PREENCHIMENTO DE CAMPO',
                        'Para realizar a busca � necess�rio preencher o c�digo do Produto!',
                        'OK' );
    edt_codigo_produto.SetFocus;
    Exit;
  end;

end;

procedure Tform_pedido_venda.btn_excluir_pedidoClick(Sender: TObject);
begin
  try
    form_consulta_vendas := tForm_Consulta_Vendas.Create(Self);
    form_consulta_vendas.ShowModal;
  finally
    Form_produtos.Free;
  end;
end;

procedure Tform_pedido_venda.btn_gravar_pedidoClick(Sender: TObject);
var
  Erro : String;
  Operacao :string;
begin
  form_dados.Pedido_Venda.Pedido_Valor_Total := StrToFloat(edt_total.Text);



  if form_dados.Pedido_Venda.Numero_Pedido > 0 then
    operacao := 'ALTERAR'
  else
    operacao := 'INSERIR';

  if form_dados.Pedido_Venda.fnc_inserir_alterar_Pedido(Operacao, Erro) then
  begin
    fnc_criar_mensagem( 'INSERINDO DADOS',
                        'CADASTRAR/ALTERAR CLIENTE',
                        'Dados Gravados com Sucesso!',
                        'OK' );
    edt_nome_cliente.SetFocus;
  end else
  begin
    fnc_criar_mensagem( 'CONFIRMANDO DADOS',
                        'ERRO AO CADASTRAR/ALTERAR CLIENTE',
                        'N�o foi poss�vel Cadastrar o Cliente, poss�vel causa: ' + Erro,
                        'OK' );
    edt_nome_cliente.SetFocus;
  end;
end;

procedure Tform_pedido_venda.btn_incluir_clientesClick(Sender: TObject);
begin
  try
    form_clientes:= tForm_Clientes.Create(Self);
    form_clientes.ShowModal;
  finally
    Form_clientes.Free;
  end;
end;

procedure Tform_pedido_venda.btn_incluir_produtosClick(Sender: TObject);
begin
  try
    form_produtos := tForm_Produtos.Create(Self);
    form_produtos.ShowModal;
  finally
    Form_produtos.Free;
  end;
end;

procedure Tform_pedido_venda.btn_inserirClick(Sender: TObject);
var
  Erro : String;
  Operacao : String;
  Qtd : Integer;
  valor_total_produto : Double;
  valor_total_pedido : Double;
  i : integer;

begin
  prc_validar_campos_obrigatorios(form_pedido_venda);

  if edt_quantidade.text = '' then
    qtd := 0;
  qtd := StrToInt(edt_quantidade.text);

  if qtd < 1 then
    begin
      fnc_criar_mensagem( 'ATEN��O',
                          'VERIFICAR CAMPOS',
                          'Digite uma quantidade v�lida',
                          'OK' );
      Exit;
    end;

  valor_total_produto := StrToFloat(edt_quantidade.Text) * StrToFloat(edt_valor_unitario.Text);

  form_dados.Pedido_Venda.Numero_Pedido           := 25;
  form_dados.Pedido_Venda.Pedido_Data_Emissao     := date();
  form_dados.Pedido_Venda.Pedido_Cliente_Codigo   := StrToInt(edt_codigo_cliente.Text);
  form_dados.Pedido_Venda.Produtos_Codigo         := StrToInt(edt_codigo_produto.Text);
  form_dados.Pedido_Venda.Produtos_Quantidade     := StrToFloat(edt_quantidade.Text);
  form_dados.Pedido_Venda.Produtos_Descricao      := edt_produto_descricao.Text;
  form_dados.Pedido_Venda.Produtos_Valor_Unitario := StrToFloat(edt_valor_unitario.Text);
  form_dados.Pedido_Venda.Produtos_Valor_Total    := valor_total_produto;

  if form_dados.Pedido_Venda.Autoincremento > 0 then
    operacao := 'ALTERAR'
  else
    operacao := 'INSERIR';

  if form_dados.Pedido_Venda.fnc_inserir_alterar(Operacao, Erro) then
  begin
    fnc_criar_mensagem( 'INSERINDO DADOS',
                        'CADASTRAR/ALTERAR PEDIDO DE VENDA',
                        'Dados Gravados com Sucesso!',
                        'OK' );
    edt_codigo_produto.SetFocus;
    edt_codigo_produto.Text   := '';
    edt_quantidade.Text       := '';
    edt_valor_unitario.Text   := '';
    Ds_Pedido_Venda.DataSet := Form_dados.Pedido_Venda.fnc_consulta('1');
  end else
  begin
    fnc_criar_mensagem( 'CONFIRMANDO DADOS',
                        'ERRO AO CADASTRAR/ALTERAR PEDIDO DE VENDA',
                        'N�o foi poss�vel Cadastrar o Pedido de Venda, poss�vel causa: ' + Erro,
                        'OK' );
    edt_codigo_produto.SetFocus;
  end;
end;


procedure Tform_pedido_venda.edt_codigo_clienteExit(Sender: TObject);
begin
  If Edt_codigo_cliente.text = '' then
  begin
    pnl_btn_consultar_pedido.Enabled := True;
    pnl_btn_consultar_pedido.Visible := True;
    pnl_btn_excluir_pedido.Enabled   := True;
    pnl_btn_excluir_pedido.Visible   := True;
  end
  else
  begin
    pnl_btn_consultar_pedido.Enabled := False;
    pnl_btn_consultar_pedido.Visible := False;
    pnl_btn_excluir_pedido.Enabled   := False;
    pnl_btn_excluir_pedido.Visible   := False;
  end;
end;

procedure Tform_pedido_venda.edt_codigo_clienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_codigo_produto.Text );
end;

procedure Tform_pedido_venda.edt_codigo_produtoKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_codigo_cliente.Text );
end;

procedure Tform_pedido_venda.edt_quantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_quantidade.Text );
end;

procedure Tform_pedido_venda.edt_valor_unitarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_valor_unitario.Text);
end;

procedure Tform_pedido_venda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  form_dados.Clientes.Destroy;
  form_dados.Produtos.Destroy;
  form_dados.Pedido_Venda.Destroy;
  Action := caFree;
end;

procedure Tform_pedido_venda.FormCreate(Sender: TObject);
begin
  form_dados.Clientes := TClientes.Create(form_dados.FDConnection);
  form_dados.Produtos := TProdutos.Create(form_dados.FDConnection);
  form_dados.Pedido_Venda := TPedido_Venda.Create(form_dados.FDConnection);

end;

procedure Tform_pedido_venda.FormShow(Sender: TObject);
begin

  lbl_numero_pedido.Caption := 'N� ' + numero_pedido.ToString;
  ds_clientes.DataSet := form_dados.Clientes.fnc_consulta('');
  ds_produtos.DataSet := form_dados.Produtos.fnc_consulta('');
  Ds_Produtos_Venda.DataSet := Form_dados.Pedido_Venda.fnc_consulta_pedidos(Numero_pedido.ToString, true);
end;


end.
