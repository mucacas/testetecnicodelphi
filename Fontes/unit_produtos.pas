unit unit_produtos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.Imaging.pngimage,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  unit_dados, classe.produtos;

type
  Tform_produtos = class(TForm)
    pnl_fundo: TPanel;
    pnl_topo: TPanel;
    lbl_titulo: TLabel;
    lbl_subtitulo: TLabel;
    img_icone: TImage;
    pnl_botoes: TPanel;
    pnl_btn_confirmar: TPanel;
    btn_confirmar: TSpeedButton;
    pnl_cancelar: TPanel;
    btn_cancelar: TSpeedButton;
    pnl_linha_topo: TPanel;
    pnl_cadastro_produto: TPanel;
    lbl_descricao_produto: TLabel;
    lbl_preco_venda: TLabel;
    Label1: TLabel;
    pnl_edt_descricao_produto: TPanel;
    edt_descricao_produto: TEdit;
    pnl_edt_preco_venda: TPanel;
    edt_preco_venda: TEdit;
    dbg_Registros: TDBGrid;
    shp_fundo: TShape;
    Ds_Produtos: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure dbg_RegistrosDblClick(Sender: TObject);
    procedure btn_confirmarClick(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);
    procedure dbg_RegistrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_preco_vendaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_produtos: Tform_produtos;

implementation

uses unit_funcoes;

{$R *.dfm}

procedure Tform_produtos.btn_cancelarClick(Sender: TObject);
begin
  close;
end;

procedure Tform_produtos.btn_confirmarClick(Sender: TObject);
var
  Erro : String;
  Operacao :string;
begin
  prc_validar_campos_obrigatorios(form_produtos);

  form_dados.Produtos.Descricao   := edt_descricao_produto.Text;
  form_dados.Produtos.Preco       := edt_preco_venda.Text;
  if form_dados.Produtos.Codigo > 0 then
    operacao := 'ALTERAR'
  else
    operacao := 'INSERIR';

  if form_dados.Produtos.fnc_inserir_alterar(Operacao, Erro) then
  begin
    fnc_criar_mensagem( 'INSERINDO DADOS',
                        'CADASTRAR/ALTERAR PRODUTO',
                        'Dados Gravados com Sucesso!',
                        'OK' );
    edt_descricao_produto.SetFocus;
    edt_descricao_produto.Text :='';
    edt_preco_venda.Text       :='';

  end else
  begin
    fnc_criar_mensagem( 'CONFIRMANDO DADOS',
                        'ERRO AO CADASTRAR/ALTERAR PRODUTO',
                        'N�o foi poss�vel Cadastrar o Produto, poss�vel causa: ' + Erro,
                        'OK' );
    edt_descricao_produto.SetFocus;
  end;
end;


procedure Tform_produtos.dbg_RegistrosDblClick(Sender: TObject);
begin
  if(Not (Dbg_registros.DataSource.DataSet.IsEmpty))then
  begin
    edt_descricao_produto.Text := dbg_registros.Datasource.dataset.FieldByName('produtos_descricao').AsString;
    edt_preco_venda.Text       := dbg_registros.Datasource.dataset.FieldByName('produtos_preco_venda').AsString;

    form_dados.Produtos.Codigo := dbg_registros.Datasource.dataset.FieldByName('produtos_codigo').AsInteger;
  end;
end;

procedure Tform_produtos.dbg_RegistrosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Not (Dbg_registros.DataSource.DataSet.IsEmpty) and(key = VK_DELETE ))then
    form_dados.Produtos.prc_deleta(dbg_registros.DataSource.DataSet.FieldByName('produtos_codigo').AsInteger)
end;

procedure Tform_produtos.edt_preco_vendaKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_preco_venda.Text );
end;

procedure Tform_produtos.FormCreate(Sender: TObject);
begin
  form_dados.Produtos := TProdutos.Create(form_dados.FDConnection);
end;

procedure Tform_produtos.FormShow(Sender: TObject);
begin
  ds_Produtos.DataSet := Form_dados.Produtos.fnc_consulta('');
end;

end.
