unit unit_configurar_servidor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage, Vcl.Buttons;

type
  Tform_configurar_servidor = class(TForm)
    shp_fundo: TShape;
    pnl_topo: TPanel;
    pnl_config_nova: TPanel;
    pnl_config_atual: TPanel;
    pnl_fundo: TPanel;
    lbl_titulo: TLabel;
    lbl_subtitulo: TLabel;
    img_icone: TImage;
    pnl_botoes: TPanel;
    pnl_btn_confirmar: TPanel;
    btn_confirmar: TSpeedButton;
    pnl_cancelar: TPanel;
    btn_cancelar: TSpeedButton;
    lbl_titulo_config_nova: TLabel;
    lbl_titulo_config_atual: TLabel;
    pnl_linha_config_nova: TPanel;
    pnl_linha_config_atual: TPanel;
    Panel1: TPanel;
    edt_caminho: TEdit;
    Panel2: TPanel;
    edt_nome_base: TEdit;
    Panel3: TPanel;
    edt_login: TEdit;
    Panel4: TPanel;
    edt_senha: TEdit;
    Panel5: TPanel;
    edt_porta: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel6: TPanel;
    edt_caminho_atual: TEdit;
    Label7: TLabel;
    Panel7: TPanel;
    edt_nome_base_atual: TEdit;
    Label8: TLabel;
    Panel8: TPanel;
    edt_login_atual: TEdit;
    Label9: TLabel;
    Panel9: TPanel;
    edt_porta_atual: TEdit;
    Label10: TLabel;
    Panel10: TPanel;
    edt_senha_atual: TEdit;
    procedure btn_confirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_configurar_servidor: Tform_configurar_servidor;

implementation

{$R *.dfm}

uses unit_dados, unit_funcoes;

procedure Tform_configurar_servidor.btn_cancelarClick(Sender: TObject);
begin
  close;
end;

procedure Tform_configurar_servidor.btn_confirmarClick(Sender: TObject);
begin
  prc_validar_campos_obrigatorios(Form_configurar_servidor);

  form_dados.Conexao.Servidor := edt_caminho.Text;
  form_dados.Conexao.Base     := edt_nome_base.Text;
  form_dados.Conexao.Login    := edt_login.Text;
  form_dados.Conexao.Senha    := edt_senha.Text;
  form_dados.Conexao.Porta    := edt_porta.Text;

  form_dados.Conexao.prc_Gravar_Arquivo_INI;

  if form_dados.Conexao.fnc_conectar_banco_dados then
  begin
    fnc_criar_mensagem( 'CONEX�O AO BANCO DE DADOS',
                        'CONECTADO AO BANCO DE DADOS',
                        'Conex�o com Banco de Dados Realizada com Sucesso!'+
                        ' O Sistema deve ser reiniciado!',
                        'OK' );
    Application.Terminate;
  end else
  begin
    fnc_criar_mensagem( 'CONEX�O AO BANCO DE DADOS',
                        'ERRO AO CONECTAR AO BANCO DE DADOS',
                        'N�o foi poss�vel conectar ao Banco de Dados, poss�vel causa: ' + form_dados.Conexao.MsgErro,
                        'OK' );
    edt_caminho.SetFocus;
  end;

end;

procedure Tform_configurar_servidor.FormShow(Sender: TObject);
begin
  if form_dados.Conexao.fnc_Ler_Arquivo_INI then
    begin
      edt_caminho_atual.Text   := form_dados.Conexao.Servidor;
      edt_nome_base_atual.Text := form_dados.Conexao.Base;
      edt_login_atual.Text     := form_dados.Conexao.Login;
      edt_senha_atual.Text     := form_dados.Conexao.Senha;
      edt_porta_atual.Text     := form_dados.Conexao.Porta;
    end;
end;

end.
