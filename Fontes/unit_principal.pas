unit unit_principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Vcl.StdCtrls;

type
  Tform_principal = class(TForm)
    pnl_topo: TPanel;
    lbl_titulo: TLabel;
    img_logo: TImage;
    pnl_botoes: TPanel;
    pnl_btn_minimizar: TPanel;
    btn_minimizar: TSpeedButton;
    pnl_fechar: TPanel;
    btn_fechar: TSpeedButton;
    pnl_fundo: TPanel;
    pnl_main: TPanel;
    pnl_rodape: TPanel;
    Label6: TLabel;
    pnl_linha_rodape: TPanel;
    pnl_logo_cliente: TPanel;
    pnl_logo: TPanel;
    pnl_menu: TPanel;
    lbl_titulo_menu: TLabel;
    pnl_linha_menu: TPanel;
    pnl_btn_servidor: TPanel;
    shp_servidor: TShape;
    btn_servidor: TSpeedButton;
    pnl_pedido_venda: TPanel;
    shp_pedido_venda: TShape;
    btn_pedido_venda: TSpeedButton;
    lbl_usuario_logado: TLabel;
    pnl_btn_clientes: TPanel;
    shp_clientes: TShape;
    btn_clientes: TSpeedButton;
    pnl_btn_produtos: TPanel;
    shp_produtos: TShape;
    btn_produtos: TSpeedButton;
    procedure btn_fecharClick(Sender: TObject);
    procedure btn_minimizarClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure btn_pedido_vendaClick(Sender: TObject);
    procedure btn_servidorClick(Sender: TObject);
    procedure btn_clientesClick(Sender: TObject);
    procedure btn_produtosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_principal: Tform_principal;

implementation

{$R *.dfm}

uses unit_pedido_venda, unit_configurar_servidor, unit_clientes, unit_produtos;

procedure Tform_principal.btn_fecharClick(Sender: TObject);
begin
  application.terminate;
end;

procedure Tform_principal.btn_minimizarClick(Sender: TObject);
begin
  application.Minimize;
end;

procedure Tform_principal.btn_pedido_vendaClick(Sender: TObject);
begin
  form_pedido_venda        := Tform_pedido_venda.Create(Self);
  form_pedido_venda.Parent := pnl_logo_cliente;
  form_pedido_venda.Show;

  pnl_menu.Enabled         :=False;
end;

procedure Tform_principal.btn_produtosClick(Sender: TObject);
begin
  try
    form_produtos:= tForm_produtos.Create(Self);
    form_produtos.ShowModal;
  finally
    Form_produtos.Free;
  end;
end;

procedure Tform_principal.btn_servidorClick(Sender: TObject);
begin
  try
    form_configurar_servidor := Tform_configurar_servidor.Create(Self);
    form_configurar_servidor.ShowModal;
  finally
    form_configurar_servidor.free;
  end;
end;

procedure Tform_principal.FormResize(Sender: TObject);
begin
  pnl_fundo.Left := Round ( (form_principal.Width - pnl_fundo.Width) / 2 );
  pnl_fundo.Top  := Round ( (form_principal.Height - pnl_fundo.Height) / 2 );
end;

procedure Tform_principal.btn_clientesClick(Sender: TObject);
begin
  try
    form_clientes:= tForm_Clientes.Create(Self);
    form_clientes.ShowModal;
  finally
    Form_clientes.Free;
  end;
end;

end.
