unit unit_clientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  unit_dados, classe.clientes;

type
  Tform_clientes = class(TForm)
    pnl_fundo: TPanel;
    pnl_topo: TPanel;
    lbl_titulo: TLabel;
    lbl_subtitulo: TLabel;
    img_icone: TImage;
    pnl_botoes: TPanel;
    pnl_btn_confirmar: TPanel;
    btn_confirmar: TSpeedButton;
    pnl_cancelar: TPanel;
    btn_cancelar: TSpeedButton;
    pnl_cadastro_cliente: TPanel;
    lbl_nome: TLabel;
    lbl_cidade: TLabel;
    lbl_uf: TLabel;
    Panel11: TPanel;
    pnl_edt_nome: TPanel;
    edt_nome_cliente: TEdit;
    pnl_edt_cidade: TPanel;
    edt_cidade: TEdit;
    pnl_edt_uf: TPanel;
    edt_uf: TEdit;
    dbg_Registros: TDBGrid;
    Ds_Clientes: TDataSource;
    Label1: TLabel;
    shp_fundo: TShape;
    procedure btn_cancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbg_RegistrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn_confirmarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dbg_RegistrosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_clientes: Tform_clientes;

implementation

{$R *.dfm}

uses unit_funcoes;

procedure Tform_clientes.btn_cancelarClick(Sender: TObject);
begin
  close;
end;

procedure Tform_clientes.btn_confirmarClick(Sender: TObject);
var
  Erro : String;
  Operacao :string;
begin
  prc_validar_campos_obrigatorios(form_clientes);

  form_dados.Clientes.Nome   := edt_nome_cliente.Text;
  form_dados.Clientes.Cidade := edt_cidade.Text;
  form_dados.Clientes.Uf     := edt_uf.Text;
  if form_dados.Clientes.Codigo > 0 then
    operacao := 'ALTERAR'
  else
    operacao := 'INSERIR';

  if form_dados.Clientes.fnc_inserir_alterar(Operacao, Erro) then
  begin
    fnc_criar_mensagem( 'INSERINDO DADOS',
                        'CADASTRAR/ALTERAR CLIENTE',
                        'Dados Gravados com Sucesso!',
                        'OK' );
    edt_nome_cliente.SetFocus;
    edt_nome_cliente.Text :='';
    edt_cidade.Text       :='';
    edt_uf.Text           :='';
  end else
  begin
    fnc_criar_mensagem( 'CONFIRMANDO DADOS',
                        'ERRO AO CADASTRAR/ALTERAR CLIENTE',
                        'N�o foi poss�vel Cadastrar o Cliente, poss�vel causa: ' + Erro,
                        'OK' );
    edt_nome_cliente.SetFocus;
  end;
end;

procedure Tform_clientes.dbg_RegistrosDblClick(Sender: TObject);
begin
  if(Not (Dbg_registros.DataSource.DataSet.IsEmpty))then
  begin
    edt_nome_cliente.Text := dbg_registros.Datasource.dataset.FieldByName('clientes_nome').AsString;
    edt_cidade.Text       := dbg_registros.Datasource.dataset.FieldByName('clientes_cidade').AsString;
    edt_uf.Text           := dbg_registros.Datasource.dataset.FieldByName('clientes_uf').AsString;

    form_dados.Clientes.Codigo := dbg_registros.Datasource.dataset.FieldByName('clientes_codigo').AsInteger;
  end;
end;

procedure Tform_clientes.dbg_RegistrosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Not (Dbg_registros.DataSource.DataSet.IsEmpty) and(key = VK_DELETE ))then
    form_dados.Clientes.prc_deleta(dbg_registros.DataSource.DataSet.FieldByName('clientes_codigo').AsInteger)
end;

procedure Tform_clientes.FormCreate(Sender: TObject);
begin
  form_dados.Clientes := TClientes.Create(form_dados.FDConnection);
end;

procedure Tform_clientes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    btn_confirmarClick(self);
end;

procedure Tform_clientes.FormShow(Sender: TObject);
begin
  ds_clientes.DataSet := Form_dados.Clientes.fnc_consulta('');
end;

end.
