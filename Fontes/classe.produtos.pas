unit classe.produtos;

interface
uses
  FireDAC.Comp.Client, unit_funcoes, System.SysUtils, Vcl.Dialogs;

type
  TProdutos = Class
    private
      Fpreco: String;
      FCodigo: Integer;
      FDescricao: String;
      FConexao: TFDConnection;

    public
      property Conexao : TFDConnection Read FConexao Write FConexao;
      property Codigo  : Integer Read FCodigo Write FCodigo;
      property Descricao    : String Read FDescricao Write FDescricao;
      property Preco  : String Read FPreco Write FPreco;

      Constructor Create (Conexao : TFDConnection);
      Destructor Destroy; Override;

      Function fnc_inserir_alterar(TipoOperacao : String; out Erro : String):boolean;
      Function fnc_consulta( texto : String): TFDQuery;
      Procedure prc_deleta( id_chave : integer );
      Function fnc_buscarProduto(texto: String): TFDQuery;
  End;
  var
    QryConsulta : TFDQuery;
implementation

{ TProdutos }

constructor TProdutos.Create(Conexao: TFDConnection);
begin
  FConexao := Conexao;

  QryConsulta := TFDQuery.Create(nil);
  QryConsulta.Connection := FConexao;
end;

destructor TProdutos.Destroy;
begin
  QryConsulta.Destroy;
  inherited;
end;

function TProdutos.fnc_consulta(texto: String): TFDQuery;
begin
  try
    //FConexao.Connected := False;
    FConexao.Connected := True;

    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add('Select produtos_codigo, produtos_descricao, produtos_preco_venda');
    QryConsulta.SQL.Add('From produtos');
    QryConsulta.SQL.Add('Where produtos_descricao like :p_texto');
    QryConsulta.ParamByName('p_texto').AsString := '%' + texto + '%';

    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;
end;

function TProdutos.fnc_inserir_alterar(TipoOperacao: String; out Erro: String): boolean;
begin
var
  QryInserir : TFDQuery;
begin
    Try
      Try
        FConexao.Connected := False;
        FConexao.Connected := True;

        QryInserir := TFDQuery.Create(nil);
        QryInserir.Connection := FConexao;

        QryInserir.Close;
        QryInserir.SQL.Clear;
        if TipoOperacao = 'INSERIR' then
        begin
          QryInserir.SQL.Add('Insert into produtos ( produtos_descricao, produtos_preco_venda)');
          QryInserir.SQL.Add(' values ( :p_produtos_descricao, :p_produtos_preco)');
        end
        else
        Begin
          QryInserir.SQL.Add('Update produtos set');
          QryInserir.SQL.Add(' produtos_descricao    = :p_produtos_descricao, ');
          QryInserir.SQL.Add(' produtos_preco_venda  = :p_produtos_preco ');
          QryInserir.SQL.Add(' Where produtos_codigo = :p_produtos_codigo');
          QryInserir.ParamByName('p_produtos_codigo').AsInteger := FCodigo;
        End;

        QryInserir.ParamByName('p_produtos_descricao').AsString := Fdescricao;
        QryInserir.ParamByName('p_produtos_preco').AsString     := Fpreco;

        QryInserir.ExecSql;

        Result := True;
        fnc_consulta('');
      Except
        on E : Exception do
        begin
          Erro:= E.Message;
          Result := False;
        end;
      End;
      Finally
        QryInserir.Destroy;
      End;
    end;
end;
Function TProdutos.fnc_buscarProduto(texto: String): TFDQuery;
begin
  try
    FConexao.Connected := False;
    FConexao.Connected := True;
    QryConsulta.Close;
    QryConsulta.SQL.Clear;
    QryConsulta.SQL.Add('Select produtos_descricao, produtos_preco_venda From produtos Where produtos_codigo like :p_texto');
    QryConsulta.ParamByName('p_texto').AsString := '%' + texto + '%';
    QryConsulta.Open;
  finally
    Result := QryConsulta;
  end;

end;

procedure TProdutos.prc_deleta(id_chave: integer);
begin
  if fnc_criar_mensagem('Confirmação','Excluir Dados', 'Tem certeza que deseja EXCLUIR esse PRODUTO?','Confirma') then
  Begin
    FConexao.Connected := False;
    FConexao.Connected := True;
    FConexao.ExecSQL('Delete from produtos where produtos_codigo = :id_chave',[id_chave]);

    fnc_consulta('');
  End;
end;


end.
