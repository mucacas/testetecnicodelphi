unit unit_consulta_vendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.Imaging.pngimage, classe.pedido_venda, unit_dados;

type
  Tform_consulta_vendas = class(TForm)
    pnl_fundo: TPanel;
    pnl_topo: TPanel;
    lbl_titulo: TLabel;
    lbl_subtitulo: TLabel;
    img_icone: TImage;
    pnl_botoes: TPanel;
    pnl_btn_confirmar: TPanel;
    btn_confirmar: TSpeedButton;
    pnl_cancelar: TPanel;
    btn_cancelar: TSpeedButton;
    Panel11: TPanel;
    pnl_consulta_cliente: TPanel;
    lbl_nome: TLabel;
    pnl_numero_pedido: TPanel;
    edt_numero_pedido: TEdit;
    dbg_Registros: TDBGrid;
    shp_fundo: TShape;
    Ds_Pedido_Venda: TDataSource;
    btn_consulta_pedido: TSpeedButton;
    lbl_ajuda: TLabel;
    procedure btn_cancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt_numero_pedidoKeyPress(Sender: TObject; var Key: Char);
    procedure btn_consulta_pedidoClick(Sender: TObject);
    procedure btn_confirmarClick(Sender: TObject);
    procedure dbg_RegistrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_consulta_vendas: Tform_consulta_vendas;

implementation

{$R *.dfm}
 uses
 unit_pedido_venda, unit_funcoes;

procedure Tform_consulta_vendas.btn_cancelarClick(Sender: TObject);
begin
  close;
end;

procedure Tform_consulta_vendas.btn_confirmarClick(Sender: TObject);
begin
  close;
end;

procedure Tform_consulta_vendas.btn_consulta_pedidoClick(Sender: TObject);
begin

  if edt_numero_pedido.Text <> '' then
  begin
    Ds_Pedido_Venda.DataSet := Form_dados.Pedido_Venda.fnc_consulta_pedidos(edt_numero_pedido.Text, True)
  end
  else
  begin
    Ds_Pedido_Venda.DataSet := Form_dados.Pedido_Venda.fnc_consulta_pedidos('', False);
  end;

end;

procedure Tform_consulta_vendas.dbg_RegistrosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if(Not (Dbg_registros.DataSource.DataSet.IsEmpty) and (key = VK_DELETE) and (edt_numero_pedido.Text <> ''))then
    form_dados.Pedido_Venda.prc_deleta(StrToInt(edt_numero_pedido.Text));
end;

procedure Tform_consulta_vendas.edt_numero_pedidoKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := Ret_Numero( Key, edt_numero_pedido.Text );
end;

procedure Tform_consulta_vendas.FormShow(Sender: TObject);
begin
  Ds_Pedido_Venda.DataSet := Form_dados.Pedido_Venda.fnc_consulta_pedidos('', False);
end;

end.
